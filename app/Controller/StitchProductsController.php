<?php
App::uses('AppController', 'Controller');
/**
 * StitchProducts Controller
 *
 * @property StitchProduct $StitchProduct
 * @property PaginatorComponent $Paginator
 */
class StitchProductsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->StitchProduct->recursive = 0;
		$this->set('stitchProducts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->StitchProduct->exists($id)) {
			throw new NotFoundException(__('Invalid stitch product'));
		}
		$options = array('conditions' => array('StitchProduct.' . $this->StitchProduct->primaryKey => $id));
		$this->set('stitchProduct', $this->StitchProduct->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->StitchProduct->create();
			if ($this->StitchProduct->save($this->request->data)) {
				$this->Session->setFlash(__('The stitch product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The stitch product could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->StitchProduct->exists($id)) {
			throw new NotFoundException(__('Invalid stitch product'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->StitchProduct->save($this->request->data)) {
				$this->Session->setFlash(__('The stitch product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The stitch product could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('StitchProduct.' . $this->StitchProduct->primaryKey => $id));
			$this->request->data = $this->StitchProduct->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->StitchProduct->id = $id;
		if (!$this->StitchProduct->exists()) {
			throw new NotFoundException(__('Invalid stitch product'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->StitchProduct->delete()) {
			$this->Session->setFlash(__('The stitch product has been deleted.'));
		} else {
			$this->Session->setFlash(__('The stitch product could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


    public function sync_shopify() {
        $this->pull_shopify_products();
        //push_shopify_products();
    }


    public function pull_shopify_products() {

        $this->StitchProduct->pull_shopify_products();

        $this->Session->setFlash(__('Done Pulling Shopify Products'));
        return $this->redirect(array('action' => 'index'));

    }

    public function push_shopify_products() {
        $this->StitchProduct->push_shopify_products();

        exit();
    }
}
