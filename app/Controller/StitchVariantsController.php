<?php
App::uses('AppController', 'Controller');
/**
 * StitchVariants Controller
 *
 * @property StitchVariant $StitchVariant
 * @property PaginatorComponent $Paginator
 */
class StitchVariantsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->StitchVariant->recursive = 0;
		$this->set('stitchVariants', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->StitchVariant->exists($id)) {
			throw new NotFoundException(__('Invalid stitch variant'));
		}
		$options = array('conditions' => array('StitchVariant.' . $this->StitchVariant->primaryKey => $id));
		$this->set('stitchVariant', $this->StitchVariant->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->StitchVariant->create();
			if ($this->StitchVariant->save($this->request->data)) {
				$this->Session->setFlash(__('The stitch variant has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The stitch variant could not be saved. Please, try again.'));
			}
		}
		$stitchProducts = $this->StitchVariant->StitchProduct->find('list');
		$this->set(compact('stitchProducts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->StitchVariant->exists($id)) {
			throw new NotFoundException(__('Invalid stitch variant'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->StitchVariant->save($this->request->data)) {
				$this->Session->setFlash(__('The stitch variant has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The stitch variant could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('StitchVariant.' . $this->StitchVariant->primaryKey => $id));
			$this->request->data = $this->StitchVariant->find('first', $options);
		}
		$stitchProducts = $this->StitchVariant->StitchProduct->find('list');
		$this->set(compact('stitchProducts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->StitchVariant->id = $id;
		if (!$this->StitchVariant->exists()) {
			throw new NotFoundException(__('Invalid stitch variant'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->StitchVariant->delete()) {
			$this->Session->setFlash(__('The stitch variant has been deleted.'));
		} else {
			$this->Session->setFlash(__('The stitch variant could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
