<?php
App::uses('AppController', 'Controller');
/**
 * StitchProducts Controller
 *
 * @property StitchProduct $StitchProduct
 * @property PaginatorComponent $Paginator
 */
class RestStitchProductsController extends AppController {
    public $uses = array('StitchProduct');
    public $helpers = array('Html', 'Form');
/**
 * Components
 *
 * @var array
 */
	public $components = array('RequestHandler');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$stitch_products = $this->StitchProduct->find('all');
        $this->set(array(
            'stitch_products' => $stitch_products,
            '_serialize' => array('stitch_products')
        ));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id) {
        $stitch_product = $this->StitchProduct->findById($id);
        $this->set(array(
            'stitch_product' => $stitch_product,
            '_serialize' => array('stitch_product')
        ));
    }

    public function sync() {
        $this->StitchProduct->pull_shopify_products();
        $this->set(array(
            'response' => 'Done syncing',
            '_serialize' => array('response')
        ));
    }

}
