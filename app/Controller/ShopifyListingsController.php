<?php
App::uses('AppController', 'Controller');
/**
 * ShopifyListings Controller
 *
 * @property ShopifyListing $ShopifyListing
 * @property PaginatorComponent $Paginator
 */
class ShopifyListingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ShopifyListing->recursive = 0;
		$this->set('shopifyListings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ShopifyListing->exists($id)) {
			throw new NotFoundException(__('Invalid shopify listing'));
		}
		$options = array('conditions' => array('ShopifyListing.' . $this->ShopifyListing->primaryKey => $id));
		$this->set('shopifyListing', $this->ShopifyListing->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ShopifyListing->create();
			if ($this->ShopifyListing->save($this->request->data)) {
				$this->Session->setFlash(__('The shopify listing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The shopify listing could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ShopifyListing->exists($id)) {
			throw new NotFoundException(__('Invalid shopify listing'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ShopifyListing->save($this->request->data)) {
				$this->Session->setFlash(__('The shopify listing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The shopify listing could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ShopifyListing.' . $this->ShopifyListing->primaryKey => $id));
			$this->request->data = $this->ShopifyListing->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ShopifyListing->id = $id;
		if (!$this->ShopifyListing->exists()) {
			throw new NotFoundException(__('Invalid shopify listing'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ShopifyListing->delete()) {
			$this->Session->setFlash(__('The shopify listing has been deleted.'));
		} else {
			$this->Session->setFlash(__('The shopify listing could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function pull_shopify_products() {

        $products = $this->ShopifyListing->get_shopify_products();

        var_dump($products["products"][0]["variants"][0]); exit();

        foreach ($products["products"] as $product) {

            foreach ($product["variants"] as $variant) {
                $this->ShopifyListing->create();
                $this->ShopifyListing->set("sku", $variant["sku"]);
                $this->ShopifyListing->set("product_name", $variant["sku"]);
                $this->ShopifyListing->set("sku", $variant["sku"]);
                $this->ShopifyListing->set("sku", $variant["sku"]);
                $this->ShopifyListing->save();
            }
        }

        $this->set('shopifyListings', $products);
    }
}
