<?php
App::uses('AppModel', 'Model');
/**
 * StitchProduct Model
 *
 * @property StitchVariant $StitchVariant
 */
class StitchProduct extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'product_name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'StitchVariant' => array(
			'className' => 'StitchVariant',
			'foreignKey' => 'stitch_product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

    /**
     * Sync down Shopify Products and Variants
     */
    public function pull_shopify_products() {
        $client = new GuzzleHttp\Client();
        $res = $client->get('https://genes-test-store-2.myshopify.com/admin/products.json', [
            'auth' =>  ['75a4f4e20170e4f12f85296b484d36cb', '4be6c0e06bbd638b6caeeff74bfab31d']
        ]);
        $products = $res->json();

        //var_dump($products["products"][0]["variants"][0]); exit();

        foreach ($products["products"] as $product) {

            $conditions = array(
                'StitchProduct.product_name' => $product["title"]
            );

            //debug($this->findByProductName($product["title"])); die();

            // Have we seen this product before?  Yes, then skip
            if (!$this->hasAny($conditions)){
                $this->create();
                $this->set("product_name", $product["title"]);
                $this->set("shopify_product_id", $product["id"]);
                $this->save();

                foreach ($product["variants"] as $variant) {
                    $conditions = array(
                        'StitchVariant.sku' => $variant["sku"]
                    );
                    if (!$this->StitchVariant->hasAny($conditions)){
                        $this->StitchVariant->create();
                    }
                    else {
                        $this->StitchVariant->findBySku($variant["sku"]);
                    }
                    $this->StitchVariant->set("stitch_product_id", $this->id);
                    $this->StitchVariant->set("sku", $variant["sku"]);
                    $this->StitchVariant->set("options", $variant["option1"]);
                    $this->StitchVariant->set("quantity", $variant["inventory_quantity"]);
                    $this->StitchVariant->set("price", $variant["price"]);
                    $this->StitchVariant->set("shopify_variant_id", $variant["id"]);
                    $this->StitchVariant->save();
                }

            }
        }
    }

    public function push_shopify_products() {
        $client = new GuzzleHttp\Client();

        $products = $this->find('all');

        foreach ($products as $product) {
            $shopify_product_id = $product["StitchProduct"]["shopify_product_id"];

            if (!empty($shopify_product_id)) {
                $res = $client->get('https://genes-test-store-2.myshopify.com/admin/products/' . $shopify_product_id . '.json', [
                    'auth' =>  ['75a4f4e20170e4f12f85296b484d36cb', '4be6c0e06bbd638b6caeeff74bfab31d']
                ]);

                if ($res->json() != null) {
                    $res = $client->put('https://genes-test-store-2.myshopify.com/admin/products/' . $shopify_product_id . '.json', [
                        'auth' =>  ['75a4f4e20170e4f12f85296b484d36cb', '4be6c0e06bbd638b6caeeff74bfab31d'],
                        'body' => ['body' => '{"product": {"id":' . $shopify_product_id . ',"title": "'. $product["StitchProduct"]['product_name'] . '"}}'
                        ]
                    ]);
                }
            }
        }

        die();
    }



}
