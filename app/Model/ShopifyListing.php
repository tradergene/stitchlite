<?php
App::uses('AppModel', 'Model');
/**
 * ShopifyListing Model
 *
 */
class ShopifyListing extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'product_name';


}
