<?php
App::uses('AppModel', 'Model');
/**
 * StitchVariant Model
 *
 * @property StitchProduct $StitchProduct
 */
class StitchVariant extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'options';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'StitchProduct' => array(
			'className' => 'StitchProduct',
			'foreignKey' => 'stitch_product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
