<div class="stitchVariants view">
<h2><?php echo __('Stitch Variant'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($stitchVariant['StitchVariant']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Stitch Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($stitchVariant['StitchProduct']['product_name'], array('controller' => 'stitch_products', 'action' => 'view', $stitchVariant['StitchProduct']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sku'); ?></dt>
		<dd>
			<?php echo h($stitchVariant['StitchVariant']['sku']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Options'); ?></dt>
		<dd>
			<?php echo h($stitchVariant['StitchVariant']['options']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($stitchVariant['StitchVariant']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($stitchVariant['StitchVariant']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($stitchVariant['StitchVariant']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($stitchVariant['StitchVariant']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Stitch Variant'), array('action' => 'edit', $stitchVariant['StitchVariant']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Stitch Variant'), array('action' => 'delete', $stitchVariant['StitchVariant']['id']), array(), __('Are you sure you want to delete # %s?', $stitchVariant['StitchVariant']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Stitch Variants'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Variant'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stitch Products'), array('controller' => 'stitch_products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Product'), array('controller' => 'stitch_products', 'action' => 'add')); ?> </li>
	</ul>
</div>
