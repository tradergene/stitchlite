<div class="stitchVariants form">
<?php echo $this->Form->create('StitchVariant'); ?>
	<fieldset>
		<legend><?php echo __('Edit Stitch Variant'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('stitch_product_id');
		echo $this->Form->input('sku');
		echo $this->Form->input('options');
		echo $this->Form->input('quantity');
		echo $this->Form->input('price');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('StitchVariant.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('StitchVariant.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Stitch Variants'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stitch Products'), array('controller' => 'stitch_products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Product'), array('controller' => 'stitch_products', 'action' => 'add')); ?> </li>
	</ul>
</div>
