<div class="stitchVariants index">
	<h2><?php echo __('Stitch Variants'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('stitch_product_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sku'); ?></th>
			<th><?php echo $this->Paginator->sort('options'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('price'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($stitchVariants as $stitchVariant): ?>
	<tr>
		<td><?php echo h($stitchVariant['StitchVariant']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($stitchVariant['StitchProduct']['product_name'], array('controller' => 'stitch_products', 'action' => 'view', $stitchVariant['StitchProduct']['id'])); ?>
		</td>
		<td><?php echo h($stitchVariant['StitchVariant']['sku']); ?>&nbsp;</td>
		<td><?php echo h($stitchVariant['StitchVariant']['options']); ?>&nbsp;</td>
		<td><?php echo h($stitchVariant['StitchVariant']['quantity']); ?>&nbsp;</td>
		<td><?php echo h($stitchVariant['StitchVariant']['price']); ?>&nbsp;</td>
		<td><?php echo h($stitchVariant['StitchVariant']['created']); ?>&nbsp;</td>
		<td><?php echo h($stitchVariant['StitchVariant']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $stitchVariant['StitchVariant']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $stitchVariant['StitchVariant']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $stitchVariant['StitchVariant']['id']), array(), __('Are you sure you want to delete # %s?', $stitchVariant['StitchVariant']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Stitch Variant'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Stitch Products'), array('controller' => 'stitch_products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Product'), array('controller' => 'stitch_products', 'action' => 'add')); ?> </li>
	</ul>
</div>
