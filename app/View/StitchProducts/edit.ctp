<div class="stitchProducts form">
<?php echo $this->Form->create('StitchProduct'); ?>
	<fieldset>
		<legend><?php echo __('Edit Stitch Product'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('product_name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('StitchProduct.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('StitchProduct.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Stitch Products'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stitch Variants'), array('controller' => 'stitch_variants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Variant'), array('controller' => 'stitch_variants', 'action' => 'add')); ?> </li>
	</ul>
</div>
