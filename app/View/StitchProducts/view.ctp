<div class="stitchProducts view">
<h2><?php echo __('Stitch Product'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($stitchProduct['StitchProduct']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product Name'); ?></dt>
		<dd>
			<?php echo h($stitchProduct['StitchProduct']['product_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($stitchProduct['StitchProduct']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($stitchProduct['StitchProduct']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Stitch Product'), array('action' => 'edit', $stitchProduct['StitchProduct']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Stitch Product'), array('action' => 'delete', $stitchProduct['StitchProduct']['id']), array(), __('Are you sure you want to delete # %s?', $stitchProduct['StitchProduct']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Stitch Products'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Product'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stitch Variants'), array('controller' => 'stitch_variants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Variant'), array('controller' => 'stitch_variants', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Stitch Variants'); ?></h3>
	<?php if (!empty($stitchProduct['StitchVariant'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Stitch Product Id'); ?></th>
		<th><?php echo __('Sku'); ?></th>
		<th><?php echo __('Options'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($stitchProduct['StitchVariant'] as $stitchVariant): ?>
		<tr>
			<td><?php echo $stitchVariant['id']; ?></td>
			<td><?php echo $stitchVariant['stitch_product_id']; ?></td>
			<td><?php echo $stitchVariant['sku']; ?></td>
			<td><?php echo $stitchVariant['options']; ?></td>
			<td><?php echo $stitchVariant['quantity']; ?></td>
			<td><?php echo $stitchVariant['price']; ?></td>
			<td><?php echo $stitchVariant['created']; ?></td>
			<td><?php echo $stitchVariant['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'stitch_variants', 'action' => 'view', $stitchVariant['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'stitch_variants', 'action' => 'edit', $stitchVariant['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'stitch_variants', 'action' => 'delete', $stitchVariant['id']), array(), __('Are you sure you want to delete # %s?', $stitchVariant['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Stitch Variant'), array('controller' => 'stitch_variants', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
