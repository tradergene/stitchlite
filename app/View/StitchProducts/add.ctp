<div class="stitchProducts form">
<?php echo $this->Form->create('StitchProduct'); ?>
	<fieldset>
		<legend><?php echo __('Add Stitch Product'); ?></legend>
	<?php
		echo $this->Form->input('product_name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Stitch Products'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Stitch Variants'), array('controller' => 'stitch_variants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Variant'), array('controller' => 'stitch_variants', 'action' => 'add')); ?> </li>
	</ul>
</div>
