<div class="stitchProducts index">
	<h2><?php echo __('Stitch Products'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('product_name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($stitchProducts as $stitchProduct): ?>
	<tr>
		<td><?php echo h($stitchProduct['StitchProduct']['id']); ?>&nbsp;</td>
		<td><?php echo h($stitchProduct['StitchProduct']['product_name']); ?>&nbsp;</td>
		<td><?php echo h($stitchProduct['StitchProduct']['created']); ?>&nbsp;</td>
		<td><?php echo h($stitchProduct['StitchProduct']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $stitchProduct['StitchProduct']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $stitchProduct['StitchProduct']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $stitchProduct['StitchProduct']['id']), array(), __('Are you sure you want to delete # %s?', $stitchProduct['StitchProduct']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Stitch Product'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Stitch Variants'), array('controller' => 'stitch_variants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stitch Variant'), array('controller' => 'stitch_variants', 'action' => 'add')); ?> </li>
        <br/>
        <li><?php echo $this->Html->link(__('Sync Products'), array('controller' => 'stitch_products', 'action' => 'sync_shopify')); ?> </li>
	</ul>
</div>
